@extends('layouts.lte')

@push('style')

@endpush

@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        {{-- <h1 class="m-0">Pangkat</h1> --}}
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('user.list') }}">User</a></li>
          <li class="breadcrumb-item active">Edit {{ $user->name }}</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    
    <div class="row justify-content-center">
      <div class="col-md-6 col-offset-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Edit User {{ $user->name }}</h3>
          </div>
          <form method="post" action="{{ route('user.update', ['id' => $user->id]) }}">
            @csrf
            <div class="card-body">

              <div class="form-group">
                <label for="name">Nama Lengkap</label>
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="Masukkan Nama Lengkap" value="{{ $user->name }}" required>
                @foreach($errors->get('name') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" id="username" name="username" placeholder="Masukkan Username" value="{{ $user->username }}" required>
                @foreach($errors->get('username') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="nip">NIP</label>
                <input type="text" class="form-control {{ $errors->has('nip') ? 'is-invalid' : '' }}" id="nip" name="nip" placeholder="Masukkan NIP" value="{{ $user->nip }}" required>
                @foreach($errors->get('nip') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="group_id">Golongan</label>
                <select name="group_id" id="group_id" class="form-control" required>
                  @foreach ($groups as $item)
                    <option value="{{ $item->id }}" {{ ($item->id == $user->group_id) ? 'selected' : '' }}>{{ $item->name }}</option>
                  @endforeach
                </select>
                @foreach($errors->get('group_id') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="rank_id">Pangkat</label>
                <select name="rank_id" id="rank_id" class="form-control" required>
                  @foreach ($ranks as $item)
                    <option value="{{ $item->id }}" {{ ($item->id == $user->rank_id) ? 'selected' : '' }}>{{ $item->name }}</option>
                  @endforeach
                </select>
                @foreach($errors->get('rank_id') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="position_id">Jabatan</label>
                <select name="position_id" id="position_id" class="form-control" required>
                  @foreach ($positions as $item)
                      <option value="{{ $item->id }}" {{ ($item->id == $user->position_id) ? 'selected' : '' }}>{{ $item->name }}</option>
                  @endforeach
                </select>
                @foreach($errors->get('position_id') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="role_id">Akses User</label>
                <select name="role_id" id="role_id" class="form-control" required>
                  <option value="2" @if($user->role_id == 2) selected @endif >User</option>
                  <option value="1" @if($user->role_id == 1) selected @endif >Admin</option>
                </select>
                @foreach($errors->get('role_id') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="password">Ganti Password</label>
                <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password" name="password" placeholder="Masukkan Password Apabila Ganti" >
                @foreach($errors->get('password') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-success">Edit</button>
            </div>
          </form>
        </div>

      </div>

    </div>

  </div>
</section>

@endsection

@push('script')

@endpush
