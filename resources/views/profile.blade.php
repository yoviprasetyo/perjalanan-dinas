@extends('layouts.lte')

@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Profile</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Profile {{ Auth::user()->name }}</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-md-6">

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Profile</h3>
          </div>

          <div class="card-body">
            <table class="table table-full">
              <tr>
                <td>Nama Lengkap</td>
                <td>{{ Auth::user()->name }}</td>
              </tr>
              <tr>
                <td>Username</td>
                <td>{{ Auth::user()->username }}</td>
              </tr>
              <tr>
                <td>NIP</td>
                <td>{{ Auth::user()->nip }}</td>
              </tr>
              <tr>
                <td>Pangkat</td>
                <td>{{ Auth::user()->rank_name }}</td>
              </tr>
              <tr>
                <td>Golongan</td>
                <td>{{ Auth::user()->group_name }}</td>
              </tr>
              <tr>
                <td>Jabatan</td>
                <td>{{ Auth::user()->position_name }}</td>
              </tr>
              <tr>
                <td>Hak Akses</td>
                <td>{{ Auth::user()->role_name }}</td>
              </tr>
            </table>
          </div>
        </div>

      </div>
    </div>

  </div>
</section>

@endsection

