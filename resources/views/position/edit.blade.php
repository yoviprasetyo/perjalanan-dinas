@extends('layouts.lte')

@push('style')

@endpush

@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        {{-- <h1 class="m-0">Jabatan</h1> --}}
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Master Data</a></li>
          <li class="breadcrumb-item"><a href="{{ route('position.list') }}">Jabatan</a></li>
          <li class="breadcrumb-item active">Ubah</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    
    <div class="row justify-content-center">
      <div class="col-md-6 col-offset-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Ubah Jabatan</h3>
          </div>
          <form method="post" action="/position/edit/{{ $position->id }}">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="name">Nama Jabatan</label>
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="Masukkan nama Jabatan" value="{{ $position->name }}">
                @foreach($errors->get('name') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Ubah</button>
            </div>
          </form>
        </div>

      </div>

    </div>

  </div>
</section>

@endsection

@push('script')

@endpush
