<!-- Sidebar -->
<div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="{{ route('profile') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'profile') === 0) ? 'active' : '' }}">
            <i class="nav-icon fa fa-user"></i>
            <p>
              Profil
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('travel.list') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'travel') === 0) ? 'active': '' }}">
            <i class="nav-icon fa fa-calendar"></i>
            <p>Perjalanan Dinas</p>
          </a>
        </li>
        @if ( Auth::user()->role_id == 1 )
        <li class="nav-item">
          <a href="{{ route('user.list') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'user') === 0) ? 'active' : '' }}">
            <i class="nav-icon fa fa-users"></i>
            <p>User</p>
          </a>
        </li>
        <li class="nav-item {{ ((strpos(Route::currentRouteName(), 'group') === 0) OR (strpos(Route::currentRouteName(), 'rank') === 0) OR (strpos(Route::currentRouteName(), 'position') === 0)) ? 'menu-is-opening menu-open' : '' }}">
          <a href="#" class="nav-link {{ ((strpos(Route::currentRouteName(), 'group') === 0) OR (strpos(Route::currentRouteName(), 'rank') === 0) OR (strpos(Route::currentRouteName(), 'position') === 0)) ? 'active' : '' }}">
            <i class="nav-icon fas fa-database"></i>
            <p>
              Master Data
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('group.list') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'group') === 0) ? 'active' : '' }}">
                <p>Golongan</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('rank.list') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'rank') === 0) ? 'active' : '' }}">
                <p>Pangkat</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('position.list') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'position') === 0) ? 'active' : '' }}">
                <p>Jabatan</p>
              </a>
            </li>
          </ul>
        </li>
        @endif
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->