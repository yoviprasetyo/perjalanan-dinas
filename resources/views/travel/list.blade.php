@extends('layouts.lte')

@push('style')

<link rel="stylesheet" href="{{ url('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ url('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ url('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Perjalanan Dinas</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active">Perjalanan Dinas</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="d-flex justify-content-between align-items-center w-100">
              <h3 class="card-title">List Perjalanan Dinas</h3>
              <div>
                <a href="{{ route('travel.add') }}" class="btn btn-sm btn-success font-weight-bold">
                  Tambah
                </a>
                <a target="_blank" href="{{ route('travel.download') }}" class="btn btn-sm btn-primary font-weight-bold">
                  Download
                </a>
              </div>
              
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="travel" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal Surat Tugas</th>
                  <th>Keperluan</th>
                  <th>Tujuan</th>
                  <th>Tanggal Mulai</th>
                  <th>Tanggal Selesai</th>
                  <th>Peserta</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @if ( count($travels) )
                @foreach ($travels as $item)
                <tr>
                <td>{{ $item->code }}</td>
                <td>{{ $item->letter_date->format('Y-m-d') }}</td>
                <td>{{ $item->necessity }}</td>
                <td>{{ $item->destination }}</td>
                <td>{{ $item->start_date->format('Y-m-d') }}</td>
                <td>{{ $item->end_date->format('Y-m-d') }}</td>
                <td>
                    <ol class="pl-3">
                    @foreach ($item->travelUsers as $travelUser)
                    <li>{{ $travelUser->user->name }}</li>
                    @endforeach
                    </ul>
                </td>
                <td>
                    <a href="{{ route('travel.show', ['id' => $item->id]) }}" class="btn btn-sm btn-primary">
                    <i class="fas fa-eye"></i>
                    </a>
                    @if ( Auth::user()->role_id == 1 )
                    <a href="{{ route('travel.update', ['id' => $item->id]) }}" class="ml-1 btn btn-sm btn-warning">
                    <i class="fas fa-edit"></i>
                    </a>
                    <button data-id="{{ $item->id }}" data-name="{{ $item->code }}" class="ml-1 btn btn-sm btn-danger delete-confirm">
                    <i class="fas fa-trash-alt"></i>
                    </button>
                    @endif
                </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7" class="text-center">Belum Ada Data</td>
                </tr>
                @endif
                
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Tanggal Surat Tugas</th>
                  <th>Keperluan</th>
                  <th>Tujuan</th>
                  <th>Tanggal Mulai</th>
                  <th>Tanggal Selesai</th>
                  <th>Peserta</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>

    </div>

  </div>
</section>

@endsection

@push('script')

<script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ url('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ url('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $('#travel').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "rowCallback": function(row, data) {
        $('.delete-confirm').click(function(event) {
        var id = $(this).data('id');
        var name = $(this).data('name');
        event.preventDefault();
        Swal.fire({
          title: `Hapus Data Perjalanan No. Surat "${name}"?`,
          text: 'Anda tidak akan dapat mengembalikan ini!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Ya, Hapus!',
          cancelButtonText: 'Batal',
          reverseButtons: true,
        }).then(result => {
          if (result.isConfirmed) {
            $.ajax({
              url: `/travel/delete/${id}`,
              type: 'POST',
              data: {
                id: id,
                '_token': '{{ csrf_token() }}',
              },
              success: function () {
                Swal.fire({
                  title: 'Success',
                  text: 'Hapus Perjalanan Dinas!',
                  icon: 'success',
                  dangerMode: true,
                }).then(function() {
                  location.reload();
                });
              },
              error: function (xhr, status, error) {
                var err = JSON.parse(xhr.responseText);
                Swal.fire({
                  title: 'Error',
                  text: err?.Message,
                  icon: 'error',
                });
              }
            })
          }
        });
      })
      }
    });
  });

</script>
@endpush
