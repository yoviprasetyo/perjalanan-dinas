@extends('layouts.lte')

@push('style')

@endpush

@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        {{-- <h1 class="m-0">Pangkat</h1> --}}
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('travel.list') }}">Perjalanan Dinas</a></li>
          <li class="breadcrumb-item active">Perjalanan Dinas {{ $travel->code }}</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Perjalanan Dinas</h3>
          </div>
          <div class="card-body">
            <table class="table table-full">
              <tr>
                <td>No. Surat</td>
                <td>{{ $travel->code }}</td>
              </tr>
              <tr>
                <td>Tanggal Surat</td>
                <td>{{ $travel->letter_date->format('Y-m-d') }}</td>
              </tr>
              <tr>
                <td>Tanggal Mulai</td>
                <td>{{ $travel->start_date->format('Y-m-d') }}</td>
              </tr>
              <tr>
                <td>Tanggal Selesai</td>
                <td>{{ $travel->end_date->format('Y-m-d') }}</td>
              </tr>
              <tr>
                <td>Keperluan</td>
                <td>{{ $travel->necessity }}</td>
              </tr>
              <tr>
                <td>Peserta</td>
                <td>
                    <ol class="pl-3">
                        @foreach ($travel->travelUsers as $item)
                        <li>{{ $item->user->name }}</li>
                        @endforeach
                    </ol>
                </td>
              </tr>
            </table>
          </div>
        </div>

      </div>

    </div>

  </div>
</section>

@endsection
