@extends('layouts.lte')

@push('style')

@endpush

@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        {{-- <h1 class="m-0">Pangkat</h1> --}}
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('travel.list') }}">Perjalanan Dinas</a></li>
          <li class="breadcrumb-item active">Tambah</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    
    <div class="row justify-content-center">
      <div class="col-md-6 col-offset-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Tambah Perjalanan Dinas</h3>
          </div>
          <form method="post" id="form-add-travel" action="{{ route('travel.add') }}">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="code">No. Surat Tugas</label>
                <input type="text" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" id="code" name="code" placeholder="Masukkan No. Surat Tugas" required>
                @foreach($errors->get('code') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="letter_date">Tanggal Surat Tugas</label>
                <input type="date" class="form-control {{ $errors->has('letter_date') ? 'is-invalid' : '' }}" id="letter_date" name="letter_date" placeholder="Masukkan Nama Lengkap" required>
                @foreach($errors->get('letter_date') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="necessity">Keperluan</label>
                <textarea class="form-control {{ $errors->has('necessity') ? 'is-invalid' : '' }}" id="necessity" name="necessity" placeholder="Masukkan Keperluan" required rows="5"></textarea>
                @foreach($errors->get('necessity') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="destination">Tujuan</label>
                <input type="text" class="form-control {{ $errors->has('destination') ? 'is-invalid' : '' }}" id="destination" name="destination" placeholder="Masukkan Tujuan" required>
                @foreach($errors->get('destination') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="start_date">Tanggal Mulai</label>
                <input type="date" class="form-control {{ $errors->has('start_date') ? 'is-invalid' : '' }}" id="start_date" name="start_date" placeholder="Masukkan Nama Lengkap" required>
                @foreach($errors->get('start_date') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div class="form-group">
                <label for="end_date">Tanggal Selesai</label>
                <input type="date" class="form-control {{ $errors->has('end_date') ? 'is-invalid' : '' }}" id="end_date" name="end_date" placeholder="Masukkan Nama Lengkap" required>
                @foreach($errors->get('end_date') as $msg)
                  <span class="text-danger text-small">{{ $msg }}</span>
                @endforeach
              </div>

              <div id="users" class="form-group d-none">
                <label for="users">Tambah Pengguna</label>

                <div id="user-list-container">
                  <table id="user-list" class="table table-hover">
                    
                  </table>
                </div>

                <button class="btn btn-sm btn-success" type="button" id="btn-add-user"  data-toggle="modal" data-target="#addUser"><i class="fas fa-plus"></i></button>
              </div>
              
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-success">Tambah</button>
            </div>
          </form>
        </div>

      </div>

    </div>

  </div>
</section>

<!-- Modal -->
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="addUserLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addUserLabel">Tambah Pengguna</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" id="form-add-user" method="post"></form>
        <p class="text-center" id="loading">Loading...</p>
        <div class="form-group d-none" id="user-input">
          <label for="user-id" class="d-block">Pengguna</label>
          <select name="user-id" id="user-id" class="form-control select2 d-block" style="width: 100%">
            
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button form="form-add-user" class="btn btn-primary">Tambah</button>
      </div>
    </div>
  </div>
</div>

@endsection

@push('script')
<script>
const users = @json($users)


const selectedUsers = []

const deleteUser  = (user) => {

  while(selectedUsers.length > 0) {
    selectedUsers.pop()
  }

  selectedUsers.forEach(item => {
    let insert  = true
    if( item.id == user.id ) {
      insert = false
    }

    if( insert ) {
      selectedUsers.push(item)
    }
  })

  showSuccess('Berhasil menghapus ' + user.name + ' dari Daftar')
  
  renderSelectedUsers()
}

const showError = (msg) => {
  $(document).Toasts('create', {
    class: 'bg-danger',
    title: 'Gagal',
    body: msg
  })
}

const showSuccess   = (msg) => {
  $(document).Toasts('create', {
    class: 'bg-success',
    title: 'Berhasil',
    body: msg
  })
}

const renderSelectedUsers = () => {

  const tableUserList  = document.querySelector('#user-list')
  tableUserList.innerHTML = ''
  
  selectedUsers.forEach(item => {
    const tr  = document.createElement('tr')
    tr.setAttribute('data-id', item.id)
    tableUserList.appendChild(tr)

    const tdName  = document.createElement('td')
    tr.appendChild(tdName)
    tdName.innerHTML  = item.name

    const tdBtn   = document.createElement('td')
    tdBtn.setAttribute('style', 'width: 15px;')
    tr.appendChild(tdBtn)

    const btn     = document.createElement('button')
    btn.setAttribute('class', 'btn btn-sm btn-danger')
    btn.setAttribute('type', 'button')
    btn.addEventListener('click', e => {
      deleteUser(item)
    })
    tdBtn.appendChild(btn)

    const i       = document.createElement('i')
    i.setAttribute('class', 'fas fa-window-close')
    btn.appendChild(i)
  })

  
}

const loadUser  = () => {
  const selectInput = document.querySelector('#user-id')
  selectInput.innerHTML = ''

  const placeHolder  = document.createElement('option')
  placeHolder.innerHTML = '-- Pilih Pengguna --'
  selectInput.appendChild(placeHolder)

  const availableUsers  = []

  users.forEach(item => {
    let insert = true
    selectedUsers.forEach(select => {
      if( item.id == select.id ) {
        insert = false
      }
    })

    if( insert ) {
      availableUsers.push(item)
    }
  })

  availableUsers.forEach(item => {
    const option  = document.createElement('option')
    option.setAttribute('value', item.id)
    option.innerHTML = item.name

    selectInput.appendChild(option)
  })

  $('#user-input').removeClass('d-none')
  $('#loading').addClass('d-none')
}

const checkTravelUser = async (startDate, endDate, user) => {
  const body = {
    start_date: startDate,
    end_date: endDate,
    user_id: user.id,
  }

  await fetch('{{ route('travel.check.travel') }}', {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    }
  })
  .then(response => response.json())
  .then(json => {
    if( json.errors !== undefined ) {
      showError(json.message)
      return
    }

    if( json.message ) {
      showSuccess(json.message)
      selectedUsers.push(user)

      $('#addUser').modal('hide')
      renderSelectedUsers()
      return
    }
  }).catch(error => {
    if( error.message ) {
      showError(error.message)
    }
  })
}

$(function () {
  $('#start_date').change(e => {
    let item  = e.target
    let value = item.value
    
    if( value ) {
      $('#users').removeClass('d-none')
    }
  })

  $('#form-add-travel').submit(e => {
    e.preventDefault()

    const formAddTravel   = document.querySelector('#form-add-travel')

    selectedUsers.forEach((item, key) => {
      const input   = document.createElement('input')
      input.setAttribute('name', 'user_id[' + key + ']')
      input.setAttribute('type', 'hidden')
      input.setAttribute('value', item.id)
      formAddTravel.appendChild(input)
    })

    formAddTravel.submit()
  })

  $('#form-add-user').submit(e => {
    e.preventDefault()
    const val = $('#user-id').val()
    const startDate = $('#start_date').val()
    const endDate   = $('#end_date').val()

    let user

    users.forEach(item => {
      if( item.id == val ) {
        user  = item
      }
    });

    checkTravelUser(startDate, endDate, user)
  })

  $('#addUser').on('show.bs.modal', e => {
    loadUser()
  })
})
</script>
@endpush
