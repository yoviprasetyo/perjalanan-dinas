<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RankController;
use App\Http\Controllers\TravelController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'formLogin'])->middleware('guest')->name('login');
Route::post('/', [AuthController::class, 'login'])->middleware('guest');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::get('/profile', [ProfileController::class, 'profile'])->middleware('auth')->name('profile');

Route::group([
    'prefix' => 'group',
    'as' => 'group.',
    'middleware' => 'auth',
], function() {
    Route::get('/list', [GroupController::class, 'list'])->name('list');
    Route::get('/add', [GroupController::class, 'formAdd'])->name('add');
    Route::post('/add', [GroupController::class, 'add']);
    Route::get('/show/{id}', [GroupController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [GroupController::class, 'formUpdate'])->name('update');
    Route::post('/edit/{id}', [GroupController::class, 'update']);
    Route::post('/delete/{id}', [GroupController::class, 'delete'])->name('delete');
});

Route::group([
    'prefix' => 'rank',
    'as' => 'rank.',
    'middleware' => 'auth',
], function() {
    Route::get('/list', [RankController::class, 'list'])->name('list');
    Route::get('/add', [RankController::class, 'formAdd'])->name('add');
    Route::post('/add', [RankController::class, 'add']);
    Route::get('/show/{id}', [RankController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [RankController::class, 'formUpdate'])->name('update');
    Route::post('/edit/{id}', [RankController::class, 'update']);
    Route::post('/delete/{id}', [RankController::class, 'delete'])->name('delete');
});

Route::group([
    'prefix' => 'position',
    'as' => 'position.',
    'middleware' => 'auth',
], function() {
    Route::get('/list', [PositionController::class, 'list'])->name('list');
    Route::get('/add', [PositionController::class, 'formAdd'])->name('add');
    Route::post('/add', [PositionController::class, 'add']);
    Route::get('/show/{id}', [PositionController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [PositionController::class, 'formUpdate'])->name('update');
    Route::post('/edit/{id}', [PositionController::class, 'update']);
    Route::post('/delete/{id}', [RankController::class, 'delete'])->name('delete');
});

Route::group([
    'prefix' => 'user',
    'as' => 'user.',
    'middleware' => 'auth',
], function() {
    Route::get('/list', [UserController::class, 'list'])->name('list');
    Route::get('/add', [UserController::class, 'formAdd'])->name('add');
    Route::post('/add', [UserController::class, 'add']);
    Route::get('/show/{id}', [UserController::class, 'show'])->name('show');
    Route::get('/edit/{id}', [UserController::class, 'formUpdate'])->name('update');
    Route::post('/edit/{id}', [UserController::class, 'update']);
    Route::post('/delete/{id}', [UserController::class, 'delete'])->name('delete');
});

Route::group([
    'prefix' => 'travel',
    'as' => 'travel.',
    'middleware' => 'auth',
], function() {
    Route::get('/list', [TravelController::class, 'list'])->name('list');
    Route::get('/download', [TravelController::class, 'download'])->name('download');
    Route::get('/show/{id}', [TravelController::class, 'show'])->name('show');
    Route::get('/add', [TravelController::class, 'formAdd'])->name('add');
    Route::post('/add', [TravelController::class, 'add']);
    Route::get('/edit/{id}', [TravelController::class, 'formUpdate'])->name('update');
    Route::post('/edit/{id}', [TravelController::class, 'update']);
    Route::post('/delete/{id}', [TravelController::class, 'delete'])->name('delete');

    Route::post('/add-user/{id}', [TravelController::class, 'addUser'])->name('add.user');
    Route::post('/remove-user/{id}', [TravelController::class, 'removeUser'])->name('remove.user');
    Route::post('/check-travel', [TravelController::class, 'checkTravel'])->name('check.travel');
    Route::post('/delete-user/{travelUserID}', [TravelController::class, 'deleteUser'])->name('delete.user');
});