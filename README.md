# Perjalanan Dinas
Aplikasi Perjalanan Dinas yang dibuat menggunakan Laravel. Dokumentasi Laravel ada [di sini](https://laravel.com/docs/8.x).

## Spesifikasi
1. PHP ^7.3
2. MySQL ^8 

## Instalasi
1. Copy file .env.example ke .env
2. Sesuaikan `DB_DATABASE`, `DB_USERNAME`, dan `DB_PASSWORD`
3. Import Database `dump.sql`
4. Aplikasi siap digunakan

## Menggunakan XAMPP
1. Buat Folder di dalam `htdocs` di XAMPP. Contoh `my-app`
2. Copy seluruh aplikasi ke folder tersebut
3. Jalankan instalasi
4. Aplikasi tersedia di `localhost/my-app/public`