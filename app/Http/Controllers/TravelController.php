<?php

namespace App\Http\Controllers;

use App\Models\Travel;
use App\Models\TravelUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class TravelController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request  = $request;
    }

    public function list()
    {
        $auth   = Auth::user();
        if( $auth->role_id == 1 ) {
            $travels    = Travel::orderBy('id', 'desc')->get();
        } else {
            $userTravels    = TravelUser::where('user_id', '=', $auth->id)->get();
            $travelIDs  = [];
            foreach ($userTravels as $key => $value) {
                array_push($travelIDs, $value->travel_id);
            }
            $travels    = Travel::whereIn('id', $travelIDs)->get();
        }

        return view('travel.list', compact('travels'));
    }

    public function download()
    {
        $auth   = Auth::user();
        if( $auth->role_id == 1 ) {
            $travels    = Travel::orderBy('id', 'desc')->get();
        } else {
            $userTravels    = TravelUser::where('user_id', '=', $auth->id)->get();
            $travelIDs  = [];
            foreach ($userTravels as $key => $value) {
                array_push($travelIDs, $value->travel_id);
            }
            $travels    = Travel::whereIn('id', $travelIDs)->get();
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getColumnDimension('A')->setWidth(15, 'px');
        $sheet->getColumnDimension('B')->setWidth(15, 'px');

        $rows   = 2;
        $cellCode           = 'C' . $rows;
        $sheet->setCellValue($cellCode, 'No. Surat Tugas');
        $cellLetterDate     = 'D' . $rows;
        $sheet->setCellValue($cellLetterDate, 'Tanggal Surat Tugas');
        $cellName           = 'E' . $rows;
        $sheet->setCellValue($cellName, 'Nama Lengkap');
        $cellNip            = 'F' . $rows;
        $sheet->setCellValue($cellNip, 'NIP');
        $cellRank           = 'G' . $rows;
        $sheet->setCellValue($cellRank, 'Pangkat');
        $cellGroup          = 'H' . $rows;
        $sheet->setCellValue($cellGroup, 'Golongan');
        $cellPosition       = 'I' . $rows;
        $sheet->setCellValue($cellPosition, 'Jabatan');
        $cellNecesity       = 'J' . $rows;
        $sheet->setCellValue($cellNecesity, 'Keperluan');
        $cellDestination    = 'K' . $rows;
        $sheet->setCellValue($cellDestination, 'Tujuan');
        $cellStartDate      = 'L' . $rows;
        $sheet->setCellValue($cellStartDate, 'Tanggal Mulai');
        $cellEndDate        = 'M' . $rows;
        $sheet->setCellValue($cellEndDate, 'Tanggal Selesai');
        $rows++;

        foreach ($travels as $item) {
            $travelUsers  = $item->travelUsers;
            foreach ($travelUsers as $travelUser) {
                $user   = $travelUser->user;
                $cellCode           = 'C' . $rows;
                $sheet->setCellValue($cellCode, $item->code);
                $cellLetterDate     = 'D' . $rows;
                $sheet->setCellValue($cellLetterDate, $item->letter_date);
                $cellName           = 'E' . $rows;
                $sheet->setCellValue($cellName, $user->name);
                $cellNip            = 'F' . $rows;
                $sheet->setCellValue($cellNip, $user->nip);
                $cellRank           = 'G' . $rows;
                $sheet->setCellValue($cellRank, $user->rank_name);
                $cellGroup          = 'H' . $rows;
                $sheet->setCellValue($cellGroup, $user->group_name);
                $cellPosition       = 'I' . $rows;
                $sheet->setCellValue($cellPosition, $user->position_name);
                $cellNecesity       = 'J' . $rows;
                $sheet->setCellValue($cellNecesity, $item->necessity);
                $cellDestination    = 'K' . $rows;
                $sheet->setCellValue($cellDestination, $item->destination);
                $cellStartDate      = 'L' . $rows;
                $sheet->setCellValue($cellStartDate, $item->start_date->format('Y-m-d'));
                $cellEndDate        = 'M' . $rows;
                $sheet->setCellValue($cellEndDate, $item->end_date->format('Y-m-d'));
                $rows++;
            }
        }

        $sheet->getStyle('C2:M' . $rows)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Perjalanan Dinas.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }

    public function show($id)
    {
        $auth   = Auth::user();
        $travel     = Travel::find($id);

        if( !$travel ) {
            return redirect( route('travel.list') )->withErrors('Travel tidak ditemukan');
        }

        if( $auth->role_id != 1 ) {
            $exist  = TravelUser::where('user_id', '=', $auth->id)->where('travel_id', '=', $id)->first();
            if( !$exist ) {
                return redirect( route('travel.list') )->withErrors('Travel tidak ditemukan');
            }
        }

        return view('travel.show', compact('travel'));
    }

    public function formAdd()
    {
        $users  = User::orderBy('name', 'ASC')->get();
        return view('travel.add', compact('users'));
    }

    public function add()
    {
        $this->validate($this->request, [
            'letter_date' => ['required'],
            'code' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'user_id' => ['required'],
        ]);

        $payloads   = $this->request->only(['letter_date', 'code', 'start_date', 'end_date', 'necessity', 'destination']);

        $userIDs    = $this->request->user_id;

        $travel     = Travel::create($payloads);

        foreach ($userIDs as $key => $id) {
            $payloads   = [
                'travel_id' => $travel->id,
                'user_id'   => $id,
            ];
            $travelUser     = TravelUser::create($payloads);
        }

        return redirect( route('travel.list') )->with('success', 'Berhasil Menambahkan Data Perjalanan Dinas');
    }
    
    public function formUpdate($id)
    {
        $travel     = Travel::find($id);
        if( !$travel ) {
            return redirect( route('travel.list') )->withErrors('Tidak ditemukan');
        }

        $travelUsers    = TravelUser::where('travel_id', '=', $id)->get();
        $userIDs    = [];
        foreach ($travelUsers as $item) {
            array_push($userIDs, $item->user_id);
        }

        $users  = User::all();

        $selectedUsers  = User::whereIn('id', $userIDs)->get();

        return view('travel.edit', compact('travel', 'users', 'selectedUsers'));
    }

    public function update($id)
    {
        $travel     = Travel::find($id);

        if( !$travel ) {
            return redirect( route('travel.list') )->withErrors('Tidak ditemukan');
        }

        $this->validate($this->request, [
            'letter_date' => ['required'],
            'code' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'user_id' => ['required'],
        ]);

        $payloads   = $this->request->only(['letter_date', 'code', 'start_date', 'end_date', 'necessity', 'destination']);

        $userIDs    = $this->request->user_id;

        $travelUsers    = $travel->travelUsers;

        foreach ($userIDs as $key => $id) {
            $insert     = true;

            foreach ($travelUsers as $travelUser) {
                if( $travelUser->user_id == $id ) {
                    $insert     = false;
                }
            }

            if( $insert ) {
                $payloads   = [
                    'travel_id' => $travel->id,
                    'user_id'   => $id,
                ];
    
                $travelUser     = TravelUser::create($payloads);
            }
        }

        $travel->update($payloads);

        return redirect( route('travel.list') )->with('success', 'Berhasil mengupdate Perjalanan Dinas');
    }

    public function delete($id)
    {
        $travel   = Travel::find($id);

        if( !$travel ) {
            return response()->json(['message' => 'Perjalanan Dinas tidak ditemukan', 'errors' => []], 404);
        }

        TravelUser::where('travel_id', '=', $id)->delete();
        $travel->delete();

        return response()->json(['message' => 'Berhasil menghapus Perjalanan Dinas']);
    }

    public function checkTravel()
    {
        $this->validate($this->request, [
            'user_id' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
        ]);

        $userID = $this->request->user_id;
        $user   = User::find($userID);

        if( !$user ) {
            return response()->json(['message' => 'User not found', 'errors' => []], 404);
        }

        $travelUsers    = TravelUser::where('user_id', '=', $userID)->get();
        $existTravelIDs = [];

        foreach ($travelUsers as $key => $value) {
            array_push($existTravelIDs, $value->travel_id);
        }

        $startDate  = $this->request->start_date;
        $endDate    = $this->request->end_date;

        $existTravel    = Travel::whereIn('id', $existTravelIDs)->where('start_date', '<=', $startDate . ' 23:59:59')->where('end_date', '>=', $startDate . ' 00:00:00')->count();

        if( $existTravel > 0 ) {
            return response()->json(['message' => $user->name . ' sudah ada perjalanan di tanggal/antara tanggal tersebut', 'errors' => []], 400);
        }

        $existTravel    = Travel::whereIn('id', $existTravelIDs)->where('start_date', '>=', $startDate . ' 23:59:59')->where('end_date', '<=', $endDate . ' 00:00:00')->count();

        if( $existTravel > 0 ) {
            return response()->json(['message' => $user->name . ' sudah ada perjalanan di tanggal/antara tanggal tersebut', 'errors' => []], 400);
        }

        $existTravel   = Travel::whereIn('id', $existTravelIDs)->where('end_date', 'LIKE', $startDate . '%')->count();

        if( $existTravel > 0 ) {
            return response()->json(['message' => $user->name . ' sudah ada perjalanan di tanggal/antara tanggal tersebut', 'errors' => []], 400);
        }

        $existTravel   = Travel::whereIn('id', $existTravelIDs)->where('start_date', 'LIKE', $endDate . '%')->count();

        if( $existTravel > 0 ) {
            return response()->json(['message' => $user->name . ' sudah ada perjalanan di tanggal/antara tanggal tersebut', 'errors' => []], 400);
        }

        return response()->json(['message' => $user->name . ' bisa ditambahkan ke perjalanan.' ]);
    }

    public function addUser($id)
    {
        $travel     = Travel::find($id);

        $this->validate($this->request, [
            'date' => ['required'],
        ]);

        $userID     = $this->request->user_id;
        $user   = User::find($userID);

        if( !$user ) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $travelUsers    = TravelUser::where('user_id', '=', $userID)->get();
        $existTravelIDs = [];

        foreach ($travelUsers as $key => $value) {
            array_push($existTravelIDs, $value->travel_id);
        }

        $startDate  = $travel->start_date;

        $existTravel    = Travel::whereIn('id', $existTravelIDs)->where('start_date', '<=', $startDate)->where('end_date', '>=', $startDate)->count();

        if( $existTravel > 0 ) {
            return response()->json(['message' => $user->name . ' sudah ada perjalanan di tanggal/antara tanggal tersebut'], 400);
        }

        $payloads   = [
            'user_id'   => $user->id,
            'travel_id' => $id,
        ];

        $travelUser     = TravelUser::create($payloads);

        return response()->json(['message' => 'Berhasil menambahkan ' . $user->name . ' ke perjalanan.' ]);
    }

    public function removeUser($id)
    {
        $travel     = Travel::find($id);
        if( !$travel ) {
            return response()->json(['message' => 'Not found', 'errors' => []], 404);
        }

        $userID     = $this->request->user_id;

        $travelUser     = TravelUser::where('travel_id', '=', $id)->where('user_id', '=', $userID)->first();
        if( !$travelUser ) {
            return response()->json(['message' => 'Not found', 'errors' => []], 404);
        }

        $travelUser->delete();

        return response()->json(['message' => 'Berhasil menghapus pengguna']);
    }

    public function deleteUser($travelUserID) 
    {
        $travelUser = TravelUser::find($travelUserID);
        if( !$travelUser ) {
            return response()->json(['message' => 'Gagal. Mohon muat ulang.'], 400);
        }

        $travelUser->delete();

        $user   = $travelUser->user;

        return response()->json(['message' => 'Berhasil menghapus ' . $user->name . ' dari perjalanan.' ]);
    }
}
