<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group;

class GroupController extends Controller
{
    public function list()
    {
        $group   = Group::all();
        return view('group.list', compact('group'));
    }

    public function formAdd()
    {
        return view('group.add');
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $payload = $request->only(['name']);

        $group = Group::create($payload);

        return redirect(route('group.list'))->with('success', 'Tambah Golongan');
    }
    
    public function formUpdate($id)
    {
        $group = Group::find($id);
        return view('group.edit', compact('group'));
    }

    public function update(Request $request, $id)
    {  
        $this->validate($request, [
            'name' => 'required'
        ]);

        $group = Group::find($id);

        $payload = $request->only(['name']);

        $group->update($payload);
        return redirect(route('group.list'))->with('success', 'Ubah Golongan');
    }

    public function delete($id)
    {
        $group = Group::find($id);
        $group->delete();
        return redirect(route('group.list'))->with('success', 'Hapus Golongan');
    }
}
