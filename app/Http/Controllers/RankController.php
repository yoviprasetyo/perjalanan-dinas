<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Rank;

class RankController extends Controller
{
    public function list()
    {
        $rank   = Rank::all();
        return view('rank.list', compact('rank'));
    }

    public function formAdd()
    {
        return view('rank.add');
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $payload = $request->only(['name']);

        $rank = Rank::create($payload);

        return redirect(route('rank.list'))->with('success', 'Tambah Pangkat');
    }
    
    public function formUpdate($id)
    {
        $rank = Rank::find($id);
        return view('rank.edit', compact('rank'));
    }

    public function update(Request $request, $id)
    {  
        $this->validate($request, [
            'name' => 'required'
        ]);

        $rank = Rank::find($id);

        $payload = $request->only(['name']);

        $rank->update($payload);
        return redirect(route('rank.list'))->with('success', 'Ubah Pangkat');
    }

    public function delete($id)
    {
        $rank = Rank::find($id);
        $rank->delete();
        return redirect(route('rank.list'))->with('success', 'Hapus Pangkat');
    }
}
