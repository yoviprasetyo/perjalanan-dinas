<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request  = $request;
    }

    public function formLogin()
    {
        return view('auth.login');
    }

    public function login()
    {
        $this->validate($this->request, [
            'username' => ['required'],
            'password' => ['required'],
        ]);

        $payloads   = $this->request->only(['username', 'password']);

        $user   = User::where('username', 'like', $this->request->username)->first();
        if( !$user ) {
            return redirect( route('login') )->withErrors('Username/Password salah');
        }

        if( !Auth::attempt($payloads) ) {
            return redirect( route('login') )->withErrors('Username/Password salah');
        }

        return redirect( route('profile') );
    }

    public function logout()
    {
        $this->request->session()->invalidate();

        $this->request->session()->regenerateToken();

        return redirect( route('login') );
    }
}
