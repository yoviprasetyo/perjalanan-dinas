<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Position;

class PositionController extends Controller
{   
    public function list()
    {
        $position   = Position::all();
        return view('position.list', compact('position'));
    }

    public function formAdd()
    {
        return view('position.add');
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $payload = $request->only(['name']);

        $position = Position::create($payload);

        return redirect(route('position.list'))->with('success', 'Tambah  Jabatan');
    }
    
    public function formUpdate($id)
    {
        $position = Position::find($id);
        return view('position.edit', compact('position'));
    }

    public function update(Request $request, $id)
    {  
        $this->validate($request, [
            'name' => 'required'
        ]);

        $position = Position::find($id);

        $payload = $request->only(['name']);

        $position->update($payload);
        return redirect(route('position.list'))->with('success', 'Ubah Posisi');
    }

    public function delete($id)
    {
        $position = Position::find($id);
        $position->delete();
        return redirect(route('position.list'))->with('success', 'Hapus Posisi');
    }
}
