<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    use HasFactory;

    protected $fillable     = [
        'code',
        'necessity',
        'underlying',
        'letter_date',
        'start_date',
        'end_date',
        'destination',
    ];

    protected $casts    = [
        'letter_date' => 'datetime',
        'start_date' => 'datetime',
        'end_date'  => 'datetime',
    ];

    public function travelUsers() {
        return $this->hasMany(TravelUser::class, 'travel_id');
    }
}
