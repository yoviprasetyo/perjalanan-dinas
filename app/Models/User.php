<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'nip',
        'role_id',
        'group_id',
        'position_id',
        'rank_id',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rank() {
        return $this->belongsTo(Rank::class, 'rank_id');
    }

    public function group() {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function position() {
        return $this->belongsTo(Position::class, 'position_id');
    }

    public function getRankNameAttribute() {
        $data   = $this->rank()->first();
        if( !$data ) {
            return 'Belum ada data';
        }

        return $data->name;
    }

    public function getGroupNameAttribute() {
        $data   = $this->group()->first();
        if( !$data ) {
            return 'Belum ada data';
        }

        return $data->name;
    }

    public function getPositionNameAttribute() {
        $data   = $this->position()->first();
        if( !$data ) {
            return 'Belum ada data';
        }

        return $data->name;
    }

    public function getRoleNameAttribute() {
        if( $this->role_id == 1 ) {
            return 'Admin';
        }
        return 'User';
    }
}
