<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TravelUser extends Model
{
    use HasFactory;

    protected $fillable     = [
        'travel_id',
        'user_id',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function travel() {
        return $this->belongsTo(Travel::class, 'travel_id');
    }
}
