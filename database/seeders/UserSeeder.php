<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin  = ['username' => 'admin', 'name' => 'Admin', 'nip' => 'admin', 'role_id' => 1];

        $user   = User::firstOrCreate($admin);
        $user->update(['password' => bcrypt('JalanDinas')]);
    }
}
